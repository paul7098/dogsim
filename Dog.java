public class Dog {
    private String name;
    private int energy;
    private int hunger;
    private int  boredom;

    public Dog(String name) {
            this.energy = 0;
            this.hunger = 0;
            this.boredom = 0;
    }

    public boolean takeNap() {
        if (this.hunger > 50) {
            System.out.println("Dog is too hungry to nap :(");
            return false;
        }
        else if (this.boredom > 50){
            System.out.println("Dog was too bored to nap :");
            return false;
        }
        else {
            this.energy+= 20;
            this.hunger+= 5;
            this.boredom+= 10;
            return true;
        }
    }

    public void eat() {
        if (this.hunger <= 30) {
            this.hunger = 0;
        } else {
            this.hunger -= 30;
        }
        System.out.println("The dog ate.");
    }
    
    public void play() {
        if (this.boredom <= 30) {
            this.boredom = 0;
        }
        else {
            this.boredom-=30;
        }

        this.hunger+=5;
        System.out.println("Dog played with human 🐕");
    }

    public boolean playWith(Human man) {
        if (this.energy < 50) {
            System.out.println("Dog lacks the required energy :(");
            return false;
        }

        else {
            this.energy-= 50;
            this.play();
            man.play();
            return true;
        }
    }
}