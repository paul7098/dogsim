
public class Human {
    private String name;
    private double money;
    private int hunger;
    private int boredom;

    public Human(String name) {
        this.name = name;
        this.money = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean doWork() {
        if (this.hunger > 50) {
            System.out.println("The human is too tired to work");
            return false;
        }
        else if (this.boredom > 50) {
            System.out.println("The human is too bored to work");
            return false;
        }
        else {
            this.money += 20;
            this.hunger += 5;
            this.boredom += 10;
            System.out.println("The human worked successfully");
            return true;
        }
    }

    public void eat() {
        if (this.hunger <= 30) {
            this.hunger = 0;
        } else {
            this.hunger -= 30;
        }
        System.out.println("The human ate.");
    }

    public void play() {
        if (this.boredom <= 30) {
            this.boredom = 0;
        }
        else {
            this.boredom-=30;
        }

        this.hunger+=5;

        System.out.println("Dog played with human 😊");
    }


    public boolean feed(Dog dog) {
        if (this.money < 50) {
            System.out.println("The human doesn't have enough money to eat");
            return false;
        } else {
            this.money -= 50;
            System.out.println("The human bought food.");
            eat();
            dog.eat();
            return true;
        }
    }
}
